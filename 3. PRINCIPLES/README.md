# My principles

First, the private victory and then achieving the public victory. Here i have the main principles that I review before making a decission or accepting a project, change or something in my life. 

I review this list and modify it as time comes by because as we gain years, we have more experience. 

## The list

1. Putting a lot of effort at short-term means satisfaction and winning at long-term
2. If you are not happy for some days in a row, something needs to change
3. Every person in this planet looks the world their own way
4. If you keep staying in your confort zone, you'll never discover things and projects that could change your perspective and life
5. How you look means more than a thousand words
6. Smile, love, dream, laugh and help anybody you meet to do the same
7. Surround yourself with the kind of people who make you grow
8. Go big, or go HOME
9. Be proactive, analyze the situation, take a decission only after thinking it several times
10. Before changing the world, you have to change yourself
11. Don't listen to what people say, if you're sure enough it´s worth the risk, go and crush it!
12. It's easy to be self-loved when everybody supports you, but there will be times when everybody has doubt about you. In that kind of situations, keep going and prove them wrong
13. Never stop believing in yourself, everything and everyone is changable but you.
14. Keep it simple, even if it complex, make it simple to understand and make the big audience fall in love with it
15. Nothing is forever, neither bad or good
16. Start and finish the tasks, you doesn't even know if tomorrow will exist
17. Think and planify 5 years in advance
18. Execute, validation and then processes
