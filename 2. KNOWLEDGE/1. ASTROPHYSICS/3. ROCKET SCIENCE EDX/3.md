# Week 3

## **Introduction**

After the Second World War, two processes run in parallel. On the one hand, the two superpowers look at each other in a "cold" way, with mutual respect for their destructive capacity. They initiate a technological weapon race, which would produce, as a byproduct, the first orbital launch in history. Moreover, several European countries, frightened by the disasters of war, initiate a way to prevent its recurrence. Spaceflight had always been seen by many as the perfect project for cooperation between peoples. A sign that the time may be propitious for it is the fact that the very same year the humankind accessed space for the first time, some countries in Europe started a path of cooperation among them.
