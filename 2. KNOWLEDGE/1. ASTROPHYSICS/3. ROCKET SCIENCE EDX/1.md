# Unit 1: The first dreamers and visionaries


## **1.1.2 Cosmology in the Ancient Greece**

Thr ancient Greek Society with his knowledge in Philosophy and Geometry, tried to build a "theory of everything", that fit both observation and common sense within a single mental frame

The predominant theory that remained was the universe of the two spheres:
* Below the moon, for the humankind
* Above the moon, for the stars

All the theories that were developed in the Middle Age and Modern Ages, were based on the **Two Spheres Universe**

## **1.1.4 Copernicus, Kepler, Galileo and Newton**

Until the irruption of Copernicus in the Renaissance, the universe theories remained the same. 

But Copernicus, with his revolution, changed that: Adopting Ptolomeo's model and locating the Sun at the center meaned that the Aristotelian concept doesn't sustained itself

Kepler proposed a simpler description for planetary orbits: Instead of spherical epicycles, a single curve. He explained his vision formulating three empirical laws known today as Kepler's laws, in which the movement of the sun and the planets around it could be explained. Galileo suported and proved right the theories of Copernicus with the observations made with his telescope

Newton and Hooke, inspirated by Galileo's idea of the Universe, conceived that the force that move the planets should be the same that made things fall in the earth. Deducing the **Law of Universal Gravitation**

With this new laws and the momentum that Renaissance bring to science, space starts to be conceived as somewhere reachable

##**1.2.1 Space travel in literature prior to 19th century**

## **1.2.2 Birth of Science Fiction**

With the starting of the Industrial revolution, the concept of machines and the impact of explorating and understanding our planet earth, fostered the imagination of writers

Characters that have to reach new places using motion vehicles, such as the Moon that Jules Verne describe in **"From the earth to the moon"**

The concept of using technology innovation to reach inimaginable places in our universe, inspired the next generations to build and make possible the dream of space exploration, that at first, was only the concept for a sci-fy of a few genius

## **1.2.4 Space travel in the early cinema**

The books that writed Berne and H.G. Wells lead to movies that reached an even bigger public and put a real vision to the concepts explained in books. 

"From the Earth to the Moon" by Verne was the first Space Exploration book based movie, here is the link https://archive.org/details/LeVoyageDansLaLun

## **1.3.1 Tsiolkovsky**

Considered the father of Astronautics, self-educated and stayed out of the scientific mainstream for the most of his life.

He providede great theorical contributions to spaceflight, one of the fundamental ones was the so called "Tsiolkovsky equation" which realtes the change in rocket's speed with the exhaust speed of the propellant and the relation of inital and final mass of the rocket.

In 1903 he published "Exploration of Outer Space by Means of Rocet Devices" where he calculate the perfomance of a liquid propellant rocket in vacuum for the first time.

His contributions had almost no repercussion at the time, but got a recognition in early soviet era. Between 1926 and 1929, he solved the practical problem on how a rocket can leae the Earth Bound.

He also thinked about how to cool the walls of the combustion chamber that reach extreme temperatures. The pump system to feed the combustion chamber and de optimal descent trajectory of the spacecraft while returning from space.

Tsiolkovsky inspired many leading Soviet rocket engineers that contributed to the success oof the Soviet space program


## **1.3.2 Oberth** 

Herman Julius Oberth is considered the father of space rockets, influenced by Jules Verne's Books.

After leaving the medicine career, while he was in World War thinking about rocketry and designing a liquid propellant rocket.

He sought a PhD degree in astronomy and physics with works on rocketry, but it was rejected in 1922 because it was considered utopic.

Even when the scientific mainstream turning his back, he succeeded publishing his thesis privately as a book in 1923. 

He developed far fetched concepts such as: Two Step rockets, Giant Mirrors to melt the ice in polar regions, or a Space Station. He joint as a mentor in the Spaceflight Society, that played a huge role for the development of military rockets within the 30's in Germany 

## **1.3.3 Others in Europe** 

Probably, the perso who did more public the space travel thing was Robert Esnault-Pelterie. Calculating the nergy required to fly to the moon independently from Tsiolkovsky's work. In the 20's he gave a series of talks and conferences about the exploration of outer space using rocket propulsion. 

Also in 1929 proposed the idea of Balistic missile for military bombardment

Thank to this visionaries that we'd talked about, more and more SpaceFlight societies that make the dream of space being as near as it has ever been. 


## 1.4.1 Motion in space 

* $$v_e = \sqrt{\dfrac{2GM}{(R+h)}}$$
* $$v_c = \sqrt{\dfrac{GM}{(R+h)}}$$

## 1.4.2 Energy and angular momentum 

## 1.4.3 Energy and angular momentum

Conservation of the angular momentum
**mr x V = H = const**


* 1st Kepler Law: Planets move in ellipses with Sun at one focus
* 2nd Kepler's Law: Each planet sweeps an equal area per unit time A1 = A2
* 3rd Kepler's Law: The square of the orbital period of a planet around the Sun is proportional to the cube of its orbit semi-major axis a T^2 = a^3

**These pysical magnitudes are vectors:**
- The position of a satellite 
- Velocities, accelerations and forces
- The angular momentum of a satellite 

## **1.4.4 Elliptic, parabolic, and hyperbollic orbits**

Elliptic, parabolic, and hyperbolic orbits
The full solution of Newton’s equation shows that orbits in the two body problem are conic sections, with the central mass located in one of the two foci. Kepler inferred from his observations that the orbits of planets around the Sun are indeed ellipses, a particular type of conic section. The possible trajectories also include parabolas and hyperbolas.


Conic sections have many geometric properties and relations that are helpful in the study of two-body-problem orbits. Conic sections of the three families --- ellipses, parabolas and hyperbolas --- can be defined with just two parameters: the semi major axis, a, and the eccentricity, e. The figures below define these elements on the conics. The size of the orbit is given primarily by the value of a, while its shape depends on e.


In the case of elliptical orbits, a > 0, and e < 1. A higher value of the eccentricity means a more stretched ellipse. When e = 0, the ellipse becomes a circular orbit. Ellipses are the only closed, repeating orbits.
In the case of parabolas, a → ∞ and e = 1. Since a is infinite in all parabolas, it is not a useful parameter to describe their size, and hence the semi latus rectum p is used instead.
In the case of hyperbolas, a < 0, and e > 1. The negative value of a indicates that the center of the conic is on the other side of the curve as seen from the focus (compare its position with the elliptic case). The hyperbola has two branches, but only one of them is the actual trajectory of the orbiting body.

![](https://prod-edxapp.edx-cdn.org/assets/courseware/v1/8250551ff6cb82f9155f4913765ab1fe/asset-v1:UC3Mx+BIA.1x+3T2019+type@asset+block/page3-img.jpg)


The point of closest approach to the central mass (i.e., the active focus) is called the pericenter, while the point farthest away from it is called the apocenter (only meaningful in the case of an ellipse). Using the relations in the figures it is easy to see that:

𝑟𝑝=𝑎(1−𝑒);            𝑟𝑎=𝑎(1+𝑒) .


The following video shows how the eccentricity changes as we change the initial velocity vector of a satellite orbiting the Earth. In this simulation, the shape of the resulting orbit is drawn for a fixed initial position vector  𝑟0 , and increasing initial velocity  𝑣0  perpendicular to  𝑟0 . Observe that the initial position vector represents the apocenter as long as the initial velocity is lower than the circular velocity; afterward, the point becomes the pericenter. The orbit is colored in black when it is an ellipse. At the escape velocity, the orbit becomes a parabola, and beyond that value, a hyperbola (red trajectories).


Parabolic and hyperbolic orbits are important in the context of interplanetary flight. Elliptical orbits around the Earth are of course a very important particular case in astrodynamics, as all Earth satellites use one such orbit. They are typically classified according to their altitudes into

Low Earth Orbits (LEO, below 2000 km altitude),
Medium Earth Orbits (MEO, anything from 2000 to 35000 km altitude), and
Geostationary Orbits (GEO, at 35786 km altitude).
Many subtypes exist within the LEO and MEO families, depending on the eccentricity and inclination of the orbits. The GEO orbit has the particular property that a satellite left in circular orbit at that altitude will move along its trajectory at the same angular rate as the rotation of the Earth, so that the satellite remains always over the same spot on Earth’s surface.


## **1.4.5 Hands-on exercise: velocity at peri- and apocenter**
Hands-on exercise: velocity at peri- and apocenter
Using the conservation of mechanical energy  𝐸=𝑚𝑣2/2−𝐺𝑀𝑚/𝑟  and the conservation of angular momentum  𝐻=𝑚𝑟×𝑣  ,derive the vis-viva equation:

𝑣2=𝐺𝑀(2𝑟−1𝑎)  (1.1)
Here  𝑚  and  𝑀  are the masses of the satellite and the Earth, and  𝑟  and  𝑣  the position and velocity vectors of the satellite. Then, using the vis-viva equation, find the maximum and minimum velocities for an elliptic orbit of semimajor axis  𝑎  and eccentricity  𝑒 .

Give it a try yourself and attempt the derivation in a paper! do not forget to check the solution afterward to check your answer, since these are important results we will use later on.


* **HINT:** You may want to particularize the equations at apogee and perigee. The position vector and the velocity vector are perpendicular at the perigee and the apogee ( 𝑟𝑎=(1+𝑒)𝑎   and   𝑟𝑝=(1−𝑒)𝑎 ). You may also consider the geometrical relation  2𝑎=𝑟𝑎+𝑟𝑝 , where  𝑟𝑎  and  𝑟𝑝  are the radius of the apogee and the perigee, respectively.

* **SOLUTION:** 
  
Angular momentum at apogee and perigee reads  𝑚𝑟𝑎𝑣𝑎  and  𝑚𝑟𝑝𝑣𝑝  ( 𝑟  and  𝑣  are perpendicular at these special orbit positions). Since angular momentum is conserved, one has  𝑟𝑎𝑣𝑎=𝑟𝑝𝑣𝑝  or, using  2𝑎=𝑟𝑎+𝑟𝑝  ,

𝑣𝑎=𝑟𝑝2𝑎−𝑟𝑝𝑣𝑝  (1.2)

On the other hand, energy conservation particularized at the apogee and perigee reads

𝐸=𝑚2𝑣2𝑎−𝐺𝑀𝑚𝑟𝑎  (1.3)
and

𝐸=𝑚2𝑣2𝑝−𝐺𝑀𝑚𝑟𝑝  (1.4)
Subtracting both equations eliminates  𝐸 :

12(𝑣2𝑝−𝑣2𝑎)=𝐺𝑀(1𝑟𝑝−1𝑟𝑎)  (1.5)
where we have divided by  𝑚  too. The substitution of Eqs. (1.2) and  2𝑎=𝑟𝑎+𝑟𝑝  in Eq. (1.5) yields

𝑣2𝑝2(1−𝑟2𝑝(2𝑎−𝑟𝑝)2)=𝐺𝑀(1𝑟𝑝−12𝑎−𝑟𝑝)→𝑣2𝑝=𝐺𝑀𝑎(2𝑎𝑟𝑝−1)  (1.6)
The energy constant is obtained by substituting Eq. (1.6) in Eq. (1.4)

𝐸=−𝐺𝑀𝑚2𝑎  (1.7)
Finally, using this result in the conservation of energy equation gives the vis-viva equation.

As expected from the inspection of the conservation of angular momentum, the maximum (minimum) velocity happens at the perigee (apogee), where the radial distance to the Earth center is minimum (maximum). One finds

𝑣2𝑎=𝐺𝑀(2𝑟𝑎−1𝑎)=𝐺𝑀𝑎(21+𝑒−1)=𝐺𝑀𝑎1−𝑒1+𝑒 
𝑣2𝑝=𝐺𝑀(2𝑟𝑝−1𝑎)=𝐺𝑀𝑎(21−𝑒−1)=𝐺𝑀𝑎1+𝑒1−𝑒 
Taking the square root of these expressions yields the velocity at apogee and perigee. It is interesting to note that the spacecraft velocity at the perigee and apogee do not depend on the spacecraft mass.

## Contenido adicional 