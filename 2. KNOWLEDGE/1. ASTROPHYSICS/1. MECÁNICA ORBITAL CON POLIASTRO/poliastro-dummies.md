# Apuntes poliastro y mecanismo orbital

## Ley de la Gravitación Universal
 ```F = G*(m*m/r^2)```

 ## Ley fundamental de la dinámica
 ```F(vector) = m * a(vector)```

Cuando aplicamos la ley fundamental de la dinámica a la gravitación universal, podemos percibir que la aceleración no depende de la masa objetivo, tan solo de la masa 

## **3. ¿Qué es una órbita?**

* Una órbita es una trayectoria alrededor de un cuerpo celeste incluyendo la tierra 
  
* Solo pueden ser elipses(caso especial: circulo) o hipérbolas(caso especial: parábolas)

* Se describen utilizando parámetros geométricos y en ocasiones, cinemáticos

* El plano de la órbita se corta con un plano de referencia formando la línea de nodos

Necesitamos una dirección de referencia y un plano de referencia para poder medir ángulos. Cómo referencia en la experimentalización se utilizan estrellas que están a miles de años luz, ya que su ubicación es prácticamente constante con respecto a otros objetos más cercanos

## Elementos keplerianos
### Forma de la órbita
* **Semieje mayor**: para elipses, mitad de la distancia de un extremo al otro
* **Excentricidad**: medida del achatamiento (0 para círculos, menor que 1 para elipses, mayor que 1 para hipérbolas)

### Posición del plano de la órbita:

* **Inclinación*:** con respecto al plano de referencia
* **Ascensión recta del nodo ascendente:** ángulo (ascensión recta) entre la dirección de referencia y el punto(nodo) en el que el satélite cruza el plano "subiendo" (ascendente)
* **Argumento del pericentro**: ángulo(argumento) entre la línea de nodos y el punto más cercano al atractor(pericentro)

## Posición del objeto dentro de la órbita:
* **Anomalía verdadera:** ángulo(anomalía) entre el pericentro y la posición actual

## **Órbitas reales**

* Las órbitas reales se desvían respecto al modelo simplificado debido a perturbaciones
* Entre ellas: resto de cuerpos del sistema Solar, no uniformidad del atractor, presión de radiación, atmósfera (cuando la hay)...
* Es muy costoso tener todos estos factores en cuenta y hacer una predicción precisa y exacta
* Se toman medidas redundantes(ópticas,láser,GPS) y se refinan continuamente

## **4. Asteroides y cometas**
* Nos centraremos en los cuerpos cercanos a la Tierra, llamados NEOs(Near Earth Objects)

El resto práctico con python y la librería poliastro está en el jupiter