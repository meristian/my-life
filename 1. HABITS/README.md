# My habits

For my overall WellNess i have a good number of habits. They are in separated folders in this directory.

*Related to habits its the point 5. SKILLSET*


## Structure of an habit:

* **Objective**: What I want to accomplish with this habit
* **Frequency**: Daily, Monthly, Weekly, etc
* **When?**: Part of the day for the execution
* **Execution**: What you need to do to succesfully check the habit


## Habit Global Index

### 1. Sport and Gym
> *In my case, I practice basket and go to the gym the rest of the week* 
* **Objectives**: Be in form, be healthy for the main purpose of keeping my self-steem in the right point, not getting sick, be agile, etc...
* **Frequency**: 5 Days a week
* **When?**: Soon in the morning or late in the night
* **Execution**: (2 Hours) Go to the Gym, Exercise according to my exercise plan. 
* **Related SKILLSET:** 3. Sport


### 2. Daily Nutrition
>
* **Objectives**: Be healty, not having any food disorders, getting the right vitamins and nutrients. Eating enough protein so my physical form isn't decreasing.
* **Frequency**: Daily
* **When?**: Every Meal
* **Execution**: Follow the day menus
* **Related SKILLSET:** 4. Nutrition

### 3. Personal diary
>
* **Objectives**: 
    * Maintain mental stability 
    * Don't stress too much about little things
    * Keep record of my struggles and feelings, learn about them in the future 
* **Frequency**: Daily
* **When?**: At any point of the day that my mind is slaping me
* **Execution**: 
    * Write down the main struggles I had that day With the help of the Play Store App *GIT Diary*