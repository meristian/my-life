# Mi sistema de organización definido al milímetro

## **1. Espacio y objetos físicos**

### 1.1 Flujo de nuevas adquisiciones
### 1.2 Inventario
### 1.3 Distribución, almacenamiento y etiquetación
### 1.4 Decisiones de compra

## **2. Objetivos, Tareas, Trabajo y Time-Management**

### 2.1 Objetivos y cómo se trasladan a Tareas (Google Tasks)
### 2.2 Tareas y organización por prioridades (Google Calendar)
### 2.3 Criterios para aceptar o declinar solicitudes de proyectos o ideas propias
### 2.4 Revisión del tiempo que se dedica a cada ámbito (Toggl) 
### 2.5 Organización de proyectos (Trello)