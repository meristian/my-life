# Indice del SkillSet

## Para su correcto uso de este sistema de mantenimiento de habilidades, debemos hacer uso de las siguientes herramientas

The Skillset is the number of skills that i want to master or that myself thinks I do. There is two parts of having an skill: Creating it and Maintaining it (Training)


## Proceso de creación de un SkillSet
1. Creación de entrada en este índice
2. Establecer Descripción, Tiempo a emplear, Frecuencia y Beneficios del Skillset 
3. Crear tarea en Google Task
4. Establecer fecha iterativa en Google Calendar de **color "Lavanda"**
5. Establecer timer en Toggl


# Lista de Skill-Set actuales 

## 1. Mecanografía
* **Descripción:** Mantenimiento de mis habilidades de mecanografía mediante el uso de herramientas online, practicando con letras y con textos
* **Tiempo a emplear:** 30 Minutos
* **Frecuencia:** Cada 1 Semana
* **Beneficios:** Mejora de la velocidad de trabajo, optimización del tiempo. Mejoras en calidad en todo lo referente a Exámenes, Programación, Escritura, etc...


## 2. Time-Management
* **Descripción:** Revisar los sistemas de organización actuales para optimizar el tiempo invertido y los resultados obtenidos, logrando una mejora de la calidad de vida, mayor aprendizaje de campos y penetración en los objetivos de mis proyectos
* **Tiempo a emplear:** 40 Minutos
* **Frecuencia:** Cada 4 Semanas
* **Beneficios:** Evitar la tendencia a romper los sistemas de organización, coger vicios, no tener presentes los objetivos correctos, etc

## 3. Deporte

* **Descripción:** Mejorar mi disciplina deportiva, aprender ejercicios, revisar mi tabla de entrenamiento, ver mi progreso y lo que he avanzado, etc
* **Tiempo a emplear:** 1 Horas
* **Frecuencia:** Cada 4 Semanas 
* **Beneficios:** Evitar mi tendencia a abandonar mi cuerpo, saltarme dias de ejercicio, etc

## 4. Nutrición

* **Descripción:** Revisar lo que he estado comiendo, cómo ha evolucionado mi forma física y cómo mejorarlo
* **Tasks:** Creating my daily menu for the entire month
* **Tiempo a emplear:** 2 Horas
* **Frecuencia:** Cada 4 Semanas 
* **Beneficios:** Evitar mi tendencia a abandonar mi cuerpo, no comer bien, etc.


## 5. Organización y Minimalismo

* **Descripción:** Cómo organizar el espacio físico, distribuirlo, etiquetarlo y mantenerlo para optimizar el tiempo empleado en buscar algo.
* **Tiempo a emplear:** 2 Horas
* **Frecuencia:** Cada 4 Semanas 
* **Beneficios:** Evitar mi tendencia a ser desordenado, dejarlo todo tirado en los cajones, almacenar cosas innecesarias, etc.


## 6. Habilidades Sociales y Relaciones

* **Descripción:** Con el libro de Dale Carnegie "Cómo ganar amigos e influir sobre las personas" continúo recordándome como se debe de relacionar uno con las personas para hacerlas sentir valiosas, no dejarlas tiradas (un problema bastante común en mi caso) y forzar relaciones de confianza
* **Tiempo a emplear:** 40 Minutos
* **Frecuencia:** Cada 1 Semana 
* **Beneficios:** Evitar mi tendencia a romper compromisos con las personas, invertir tiempo en relaciones de una forma estúpida, ser más amable y menos egocéntrico, etc...



