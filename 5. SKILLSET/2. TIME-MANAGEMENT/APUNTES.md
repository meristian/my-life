# Organización y optimización de mi tiempo
## **Prioridad del Skill**: Muy alta
## **Frecuencia de mantenimiento**: Mensual
## **Descripción del Skill**: Revisar los sistemas de organización actuales para optimizar el tiempo invertido y los resultados obtenidos, logrando una mejora de la calidad de vida, mayor aprendizaje de campos y penetración en los objetivos de mis proyectos



# 7 Things Organized People Do That You (Probably) Don't Do

Vídeo de Thomas Frank: https://www.youtube.com/watch?v=tlFGOSEI_lo

## Apuntes al respecto 



* [x] **Don't rely solely on your brain to store information** 
  * A system that you can trust is one that:
  * 1. Ins't going to lose your data
  * 2. Is Easily accesible
  * 3. It's organized itself and has a search engine
* [ ] **Label all the things**
  * What it is 
  * Where it goes
* [x] **Build a mindfulness loop**
  * Look ahead the dependencies you got, don't be surprised by them 
* [ ] **Two is one, one is none**
  * Have backups for everything you need to work
* [ ] **Organize by experience, not a dogma**
  * Question the way you organize, and fit this model to you
* [ ] **Respect the value of *mise en place***
  * Make sure that your work enviroment is prepared before you start working
* [x] **Be deliberate about the things you own**
  * Don't keep things that aren't useful to you
  * Things you don't need consume your energy and time



## Historial de revisiones de esta **Skill**
* ### 02-11-19 : **3/7**


# How to Stop Wasting Time - 5 Useful Time Management Tips 

Vídeo de Thomas Frank: https://www.youtube.com/watch?v=xwsLuxlbY2w&t=721s

## Apuntes al respecto 

* [x] **Try using a time tracking tool**
    > I use Toogl ¡yeah!
* [ ] **Know your priorities**
    > Objetivos trimestrales 
    > Anuales
    * Two questions to know if a task is really important
    * How does my schedule look like without this commitment?
    * When I'm on my death bed, will I regret not doing this?
* [ ] **Batch your tasks**
    * All the tasks that are 'outside' or in some place at the same time
    * Group this task and kill them in one sesion 

* [ ] **Get better at saying no**
    * You need to get mentally prepared to say no
    * When you're saying **yes to someting** you're saying **no to other thing**
    * Say no in a helping and kind way
* [x] **Use dead-lines**
    > Prioridades por mes, las que utilizamos en eco swap. 
    > Las tareas a las que ponemos fecha en las que deben de hacerse
    > Tomar la costumbre de hacerlas en el mismo día
    * Discipline equals Freedom

## Historial de revisiones de esta **Skill**
* ### 02-11-19 : **2/5** 
    > Debo de mejorar en establecer prioridades, leerlas más a menudo, agrupar tareas y seleccionar mejor los proyectos en los que me meto


# How to Break Your Social Media Addiction

Vídeo de Thomas Frank: https://www.youtube.com/watch?v=QGe_cG3g6kw

## Apuntes al respecto

"The best minds of my generation are involved in ad-making"

* [x] **Turn off notifications**
* [x] **Quit the icons from your homescreen** 
* [x] **Use social media only at the computer**


## Historial de revisiones de esta **Skill**
* ### 02-11-19 : **3/3** 
> Mi adicción a las redes sociales, Instagram y todas esas plataformas desapareció. Con la rotura de mi pantalla se hizo tan innecesario volver a comprobar las redes sociales... Espero que no cambie, porque la verdad es que me siento más sano así



# How to Study with INTENSE Focus - 7 Essential Tips

Vídeo de Thomas Frank: https://www.youtube.com/watch?v=hP5TNI_2VRs&t=369s

## Apuntes al respecto

* [x] Have a single target of focus
* [x] "Unlimited possibilities are not suited for man; if they existed, his life would only dissolve in the boundless. To become strong, a man's life needs the limitations ordained by duty and voluntary accepted." - I Ching
* [x] Keep the session sacred
* [ ] Proper tools. Make time to maintain the tools. 
* [ ] Practice
* [ ] Use timers, but... leave some buffer room at the end of the timed session
* [x]"There's nothing an artists needs more- even more than excellent tools and stamina- than a deadline" -Adriana Trigiani.
* [x] Work alone. "Without great solitude, no serious work is possible"- Pablo Picasso
* [ ] Make time for recovery
* [x] Interest. Give yourself a challenge

## Historial de revisiones de esta **Skill**
* ### 02-11-19 : **6/10** 
> Ultimamente, las reglas del time-blocking realizando los correctos descansos me las he pasado un poco por el forro. Necesito retomar el hábito de centrarme en una sola tarea en un bloque de tiempo, es una de las formas de las que me he dado cuenta de que necesitaba establecer esta rutina de mantenimiento de mi Skill-Set.

# Otras cosas que también hago para organizar y optimizar mi tiempo
* [x] **Leer sólo una vez al día el eMail**
* [ ] 