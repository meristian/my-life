# Decluttering for messy people

## Descripción del SkillSet: Cómo organizar el espacio físico, distribuirlo, etiquetarlo y mantenerlo para optimizar el tiempo empleado en buscar algo. Minimizar la cantidad de productos innecesarios que se compran y simplificar los espacios de trabajo, estudio y habitabilidad

## Frecuencia de revisión: 4 semanas


### Declutering: "remove unnecessary items from (an untidy or overcrowded place)."
### Video by Matt D'Avella https://www.youtube.com/watch?v=LAtHvlPViJo


Notes:
1. **get rid off the unnecessary stuff** (one room, drawer etc. at a time)
(reduce surfaces!)
2. **systemize your things** (shelves, bins...)
3. **control the flow of new stuff** (social media...)
4. **create routines** (night, morning)

## *'The small things you make everyday will make a big difference.'*